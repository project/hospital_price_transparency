<?php

namespace Drupal\Tests\hospital_price_transparency\Kernel;

use Drupal\file\Entity\File;
use Drupal\hospital_price_transparency\Entity\HospitalPriceTransparency;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests hpt validation constraints.
 *
 * @group hospital_price_transparency
 */
class HptValidationTest extends EntityKernelTestBase {

  /**
   * Allow invalid schema to support date formatter in D9.5.
   *
   * @todo: Remove when we no longer support D9 at all.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['hospital_price_transparency', 'file'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installConfig(['hospital_price_transparency', 'file']);
  }

  /**
   * Tests the hpt validation constraints.
   */
  public function testValidation() {
    $this->setCurrentUser($this->createUser(['administer hospital price transparency', 'access content']));

    file_put_contents('public://test.xml', '<xml>my xml</xml>');
    $file_xml = File::create([
      'filename' => 'test.xml',
      'uri' => 'public://test.xml',
      'status' => TRUE,
    ]);
    $file_xml->save();

    file_put_contents('public://test.txt', 'my text');
    $file_txt = File::create([
      'filename' => 'test.txt',
      'uri' => 'public://test.txt',
      'status' => TRUE,
    ]);
    $file_txt->save();

    // Make a valid hpt.
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '123456789',
      'fid' => $file_xml->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt->validate();
    $this->assertCount(0, $violations);

    // Make hpt with bad 10-digit ein.
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '1234567890',
      'fid' => $file_xml->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('ein.0.value', $violations[0]->getPropertyPath());
    $this->assertEquals('The EIN must be a 9-digit number with no hyphens.', $violations[0]->getMessage());

    // Make another hpt with bad ein with a hyphen.
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '12-3456789',
      'fid' => $file_xml->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('ein.0.value', $violations[0]->getPropertyPath());
    $this->assertEquals('The EIN must be a 9-digit number with no hyphens.', $violations[0]->getMessage());

    // Make hpt with bad hospital_name containing a slash.
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'Burns/Memorial',
      'ein' => '123456789',
      'fid' => $file_xml->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('hospital_name.0.value', $violations[0]->getPropertyPath());
    $this->assertEquals('The Hospital name must only use letters, numbers, spaces, underscores, and hyphens.', $violations[0]->getMessage());

    // Make hpt with bad file type (txt).
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '123456789',
      'fid' => $file_txt->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('fid.0', $violations[0]->getPropertyPath());
    $this->assertEquals('Only files with the following extensions are allowed: xml csv json zip.', $violations[0]->getMessage());
  }

}
