<?php

namespace Drupal\Tests\hospital_price_transparency\Kernel;

use Drupal\file\Entity\File;
use Drupal\hospital_price_transparency\Entity\HospitalPriceTransparency;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the hpt access.
 *
 * @group hospital_price_transparency
 */
class HptAccessTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'hospital_price_transparency',
    'path_alias',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('hpt');
    $this->installEntitySchema('path_alias');
  }

  /**
   * Test entity access.
   */
  public function testHptAccess() {
    // Create an hpt.
    $this->setCurrentUser($this->createUser(['administer hospital price transparency', 'access content']));
    file_put_contents('public://test.xml', '<xml>my xml</xml>');
    $file_xml = File::create([
      'filename' => 'test.xml',
      'uri' => 'public://test.xml',
      'status' => TRUE,
    ]);
    $file_xml->save();
    $hpt = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '123456789',
      'fid' => $file_xml->id(),
      'status' => TRUE,
    ]);
    $hpt->save();

    // Admin can do everything, but there is only one revision so a revision
    // cannot be deleted or reverted.
    $expected_access = [
      'view' => TRUE,
      'update' => TRUE,
      'view revision' => TRUE,
      'view all revisions' => TRUE,
      'delete' => TRUE,
      'delete revision' => FALSE,
      'revert' => FALSE,
    ];
    foreach ($expected_access as $op => $access) {
      $this->assertSame($hpt->access($op), $access, $op);
    }

    // Make a user who can create.
    $this->setCurrentUser($this->createUser(['create hospital price transparency', 'access content']));
    $expected_access = [
      'view' => TRUE,
      'update' => FALSE,
      'view revision' => FALSE,
      'view all revisions' => FALSE,
      'delete' => FALSE,
      'delete revision' => FALSE,
      'revert' => FALSE,
    ];
    foreach ($expected_access as $op => $access) {
      $this->assertSame($hpt->access($op), $access, $op);
    }

    // Make a user who can edit.
    $this->setCurrentUser($this->createUser(['edit hospital price transparency', 'access content']));
    $expected_access = [
      'view' => TRUE,
      'update' => TRUE,
      'view revision' => TRUE,
      'view all revisions' => TRUE,
      'delete' => FALSE,
      'delete revision' => FALSE,
      'revert' => FALSE,
    ];
    foreach ($expected_access as $op => $access) {
      $this->assertSame($hpt->access($op), $access, $op);
    }

    // Make a user who can delete.
    $this->setCurrentUser($this->createUser(['delete hospital price transparency', 'access content']));
    $expected_access = [
      'view' => TRUE,
      'update' => FALSE,
      'view revision' => FALSE,
      'view all revisions' => FALSE,
      'delete' => TRUE,
      'delete revision' => FALSE,
      'revert' => FALSE,
    ];
    foreach ($expected_access as $op => $access) {
      $this->assertSame($hpt->access($op), $access, $op);
    }
  }

  /**
   * Test collection route access.
   */
  public function testCollectionAccess() {
    /** @var \Symfony\Component\Routing\Route $route */
    $route = \Drupal::service('router.route_provider')->getRouteByName('entity.hpt.collection');
    $permission = $route->getRequirement('_permission');
    $perms = explode('+', $permission);
    $this->assertTrue(in_array('administer hospital price transparency', $perms, TRUE));
    $this->assertTrue(in_array('create hospital price transparency', $perms, TRUE));
    $this->assertTrue(in_array('edit hospital price transparency', $perms, TRUE));
    $this->assertTrue(in_array('delete hospital price transparency', $perms, TRUE));
  }

}
