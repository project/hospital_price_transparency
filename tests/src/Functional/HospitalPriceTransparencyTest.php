<?php

namespace Drupal\Tests\hospital_price_transparency\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\hospital_price_transparency\Entity\HospitalPriceTransparency;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests hospital_price_transparency module.
 *
 * @group hospital_price_transparency
 */
class HospitalPriceTransparencyTest extends BrowserTestBase {

  /**
   * Allow invalid schema to support date formatter in D9.5.
   *
   * @todo: Remove when we no longer support D9 at all.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views',
    'hospital_price_transparency',
  ];

  /**
   * {@inheritdoc}
   */
  public $defaultTheme = 'stark';

  /**
   * Test essential functionality of hpt entities.
   */
  public function testHospitalPriceTransparency() {
    $this->drupalLogin($this->drupalCreateUser([], 'radmin', TRUE));

    // Make a file.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/file.csv');
    $file1 = \Drupal::service('file.repository')->writeData($contents, "public://file.csv");

    // Make an hpt that uses this file.
    $hpt1 = HospitalPriceTransparency::create([
      'label' => 'My Test HPT',
      'hospital_name' => 'BurnsMemorial',
      'ein' => '123456789',
      'fid' => $file1->id(),
      'status' => TRUE,
    ]);
    $hpt1->save();

    // View the hpt.
    $this->drupalGet('/hpt/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('copper,silicon,vanadium');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/csv; charset=UTF-8');
    // View it at its alias.
    $this->drupalGet('/hpt/1/123456789_BurnsMemorial_standardcharges.csv');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/csv; charset=UTF-8');

    // Update the hpt.
    $hpt1->set('ein','987654321');
    $hpt1->save();
    // View hpt.
    $this->drupalGet('/hpt/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('copper,silicon,vanadium');
    // View it at its old alias.
    $this->drupalGet('/hpt/1/123456789_BurnsMemorial_standardcharges.csv');
    $this->assertSession()->statusCodeEquals(404);
    // View it at its alias.
    $this->drupalGet('/hpt/1/987654321_BurnsMemorial_standardcharges.csv');
    $this->assertSession()->statusCodeEquals(200);

    // Create a new hpt.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/file.json');
    $file2 = \Drupal::service('file.repository')->writeData($contents, "public://file.json");
    $hpt2 = HospitalPriceTransparency::create([
      'label' => 'My Json HPT',
      'hospital_name' => 'JsonRegional',
      'ein' => '999999999',
      'fid' => $file2->id(),
      'status' => TRUE,
    ]);
    $hpt2->save();

    // View it.
    $this->drupalGet('/hpt/2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('my json file');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/json');
    // Delete the file and confirm we 404.
    $file2->delete();
    $this->drupalGet('/hpt/2');
    $this->assertSession()->statusCodeEquals(404);

    // View the first hpt as anonymous.
    $this->drupalLogin($this->drupalCreateUser(['access content']));
    $this->drupalGet('/hpt/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('copper,silicon,vanadium');
    $this->drupalGet('/hpt/1/987654321_BurnsMemorial_standardcharges.csv');
    $this->assertSession()->statusCodeEquals(200);

    // Confirm we can't access various routes.
    $this->drupalGet('/hpt/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/hpt/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Unpublish the hpt.
    $hpt1->set('status', FALSE);
    $hpt1->save();

    // Confirm anonymous can no longer view hpt.
    $this->drupalGet('/hpt/1');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/hpt/1/987654321_BurnsMemorial_standardcharges.csv');
    $this->assertSession()->statusCodeEquals(403);

    // Try with zip.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/xml_zip.zip');
    $valid_zip = \Drupal::service('file.repository')->writeData($contents, "public://xml_zip.zip");
    $hpt3 = HospitalPriceTransparency::create([
      'label' => 'My Zip HPT',
      'hospital_name' => 'ZippyHealth',
      'ein' => '111111111',
      'fid' => $valid_zip->id(),
      'status' => TRUE,
    ]);
    $hpt3->save();

    // The file should have gotten extracted into a new file entity.
    $this->assertSame('3', $valid_zip->id());
    $this->assertSame('4', $hpt3->get('fid')->target_id);
    $file = File::load(4);
    $this->assertSame('public://file.xml', $file->getFileUri());
    $this->assertFileExists('public://file.xml');

    $this->drupalGet('/hpt/3');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('This is xml');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/xml');
    $this->drupalGet('/hpt/3/111111111_ZippyHealth_standardcharges.xml');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/xml');

    // Replace it with a bad zip and validate.
    // Todo: figure out why the validator isn't able to open the zip if this is in a Kernel test.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/txt_zip.zip');
    $text_zip = \Drupal::service('file.repository')->writeData($contents, "public://txt_zip.zip");
    $hpt3->set('fid', $text_zip->id());
    $violations = $hpt3->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('fid', $violations[0]->getPropertyPath());
    $this->assertEquals('Uploaded zips must contain a file with the following extensions: xml csv json.', $violations[0]->getMessage());

    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/three_test_files.zip');
    $triple_zip = \Drupal::service('file.repository')->writeData($contents, "public://three_test_files.zip");
    $hpt3->set('fid', $triple_zip->id());
    $violations = $hpt3->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals('fid', $violations[0]->getPropertyPath());
    $this->assertEquals('Zip file must contain exactly one file. Uploaded zip file contains 3 files.', $violations[0]->getMessage());

    // Use zip that contains __MACOSX directory.
    // Also put zip in non-root directory.
    $contents = file_get_contents(\Drupal::service('extension.list.module')->getPath('hospital_price_transparency') . '/tests/files/zip_with_macosx.zip');
    $directory_uri = 'public://some-directory';
    \Drupal::service('file_system')->prepareDirectory($directory_uri, FileSystemInterface::CREATE_DIRECTORY);
    $valid_zip = \Drupal::service('file.repository')->writeData($contents, "public://some-directory/zip_with_macosx.zip");
    $hpt4 = HospitalPriceTransparency::create([
      'label' => 'My __MACOSX HPT',
      'contact_name' => 'Dan Flanagan',
      'contact_email' => 'dan@example.com',
      'hospital_name' => 'MACOSX_Health',
      'ein' => '909090909',
      'fid' => $valid_zip->id(),
      'status' => TRUE,
    ]);
    $violations = $hpt4->validate();
    $this->assertCount(0, $violations);
    $hpt4->save();

    // The file should have gotten extracted into a new file entity.
    $this->assertSame('7', $valid_zip->id());
    $this->assertSame('8', $hpt4->get('fid')->target_id);
    $file = File::load(8);
    $this->assertSame('public://some-directory/file_xml_tagged.xml', $file->getFileUri());
    $this->assertFileExists('public://some-directory/file_xml_tagged.xml');

    $this->drupalGet('/hpt/4');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('This file has "red" and "green" tags added so that the zip contains a __MACOSX directory.');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/xml');
    $this->drupalGet('/hpt/4/909090909_MACOSX_Health_standardcharges.xml');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/xml');

    // Consider the cms-hpt.txt file.
    $this->drupalGet('/cms-hpt.txt');
    // Assert the values for hpt 4.
    $this->assertSession()->pageTextContains('location-name: My __MACOSX HPT');
    $this->assertSession()->pageTextContains('contact-name: Dan Flanagan');
    $this->assertSession()->pageTextContains('contact-email: dan@example.com');
    $expected_source_page_url = Url::fromUserInput('/hpt/price-transparency', ['absolute' => TRUE])->toString();
    $this->assertSession()->pageTextContains('source-page-url: ' . $expected_source_page_url);
    $this->assertSession()->pageTextContains('/hpt/4/909090909_MACOSX_Health_standardcharges.xml');
    // The unpublished hpt should not be there.
    $this->assertSession()->pageTextNotContains('My Test HPT');
    // hpt 2 and 3 should be present.
    $this->assertSession()->pageTextContains('location-name: My Json HPT');
    $this->assertSession()->pageTextContains('location-name: My Zip HPT');

    // Consider the contact properties, which have default value madness.
    $this->assertSession()->pageTextNotContains('Waldo Jeffers');
    $this->assertSession()->pageTextNotContains('waldo@example.com');
    $config = \Drupal::configFactory()->getEditable('hospital_price_transparency.settings');
    $config->set('contact_name', 'Waldo Jeffers');
    $config->set('contact_email', 'waldo@example.com');
    $config->save();
    // Re-consider things.
    $this->drupalGet('/cms-hpt.txt');
    $this->assertSession()->pageTextMatchesCount(2, '#contact-name: Waldo Jeffers#');
    $this->assertSession()->pageTextMatchesCount(2, '#contact-email: waldo@example.com#');
    $this->assertSession()->pageTextContainsOnce('contact-name: Dan Flanagan');
    $this->assertSession()->pageTextContainsOnce('contact-email: dan@example.com');
    $this->assertSession()->pageTextMatchesCount(3, '#source-page-url: ' . $expected_source_page_url . '#');

    // Kick the tires on the View.
    $this->drupalGet('/hpt/price-transparency');
    $this->assertSession()->pageTextNotContains('My Test HPT');
    $this->assertSession()->pageTextContains('My Json HPT');
    $this->assertSession()->pageTextContains('My Zip HPT');
    $this->assertSession()->pageTextContains('My __MACOSX HPT');
    $this->assertSession()->pageTextMatchesCount(2, '#Waldo Jeffers#');
    $this->assertSession()->pageTextMatchesCount(2, '#waldo@example.com#');
    $this->assertSession()->pageTextContainsOnce('Dan Flanagan');
    $this->assertSession()->pageTextContainsOnce('dan@example.com');
    $this->assertSession()->linkByHrefExists('/hpt/4/909090909_MACOSX_Health_standardcharges.xml');
  }

}
