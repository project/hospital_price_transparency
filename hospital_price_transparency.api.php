<?php

/**
 * @file
 * Hooks provided by the hpt module.
 */

use Drupal\file\FileInterface;
use Drupal\hospital_price_transparency\HospitalPriceTransparencyInterface;

/**
 * Alter the array representing the crossword.
 *
 * See \Drupal\crossword\CrosswordFileParserPluginBase::parse().
 *
 * @param array $data
 *   An array representing the data to be added to cms-hpt.txt.
 * @param Drupal\hospital_price_transparency\Entity\HospitalPriceTransparency $hpt
 *   The hpt entity.
 */
function hook_hospital_price_transparency_txt_alter(array &$data, HospitalPriceTransparencyInterface $hpt) {
  // Use a link field on the entity as the source page url.
  if ($url = $hpt->get('field_source_page_url')->getString()) {
    $data['source-page-url'] = $url;
  }
}
