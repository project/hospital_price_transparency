<?php

/**
 * @file
 * Token callbacks for the hospital_price_transparency module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function hospital_price_transparency_token_info() {
  // cms.gov standards specify the filename should follow the pattern of
  // [ein]_[hospital_name]_standardcharges.[extension]
  // The [hpt:file_alias] token provides that string. It is equivalent to
  // [hpt:ein]_[hpt:hospital_name]_standardcharges.[hpt:fid:entity:extension]
  // but is obviously much simpler to use.
  $info['tokens']['hpt']['file_alias'] = [
    'name' => t('HPT File Alias'),
    'description' => t('Standardized file alias following cms.gov guidelines. If using pathauto, this token should end the pattern and you should add <code>hpt:file_alias</code> to the "safe patterns" on the pathauto settings form.'),
    'type' => 'url'
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function hospital_price_transparency_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'hpt' && !empty($data['hpt'])) {
    $hpt = $data['hpt'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'file_alias':
          $replacements[$original] = $hpt->getFileAlias();
          break;
      }
    }
  }

  return $replacements;
}
