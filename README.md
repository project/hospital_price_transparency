Linking to a Hospital Price Transparency (hpt) entity
=====================================================
Creating links to the hospital price transparency document following cms.gov
guidelines for the filename is the key problem this module exists to solve.

These are content entities, so you can link to these entities in most of
the same ways you can link to a node or a taxonomy term.

For example, you can create a menu link with `/hpt/123` as the path (where
123 is the id of the hpt entity. Or you can make the path `entity:hpt/123`.
When the link is rendered, it will use the alias of the hpt entity, which
will conform to cms.gov regulations.

Or you could output a list of hpt entities in a View. These are content
entities, so they are fully compatible with Views. Any field in the View
that is configured as "link to content" will automatically link to the hpt
file.

Or you could create an entity reference field that references hpt entities.
When configuring the field formatter for this field, you could render the hpt
as a label that is "linked to content".

Or maybe you render the hpt as a teaser. This module ships with the hpt teaser
configured and templated to display the label and the last updated date followed
by a "view" link and a "download" link.

Etc.

About the Alias
===============
One of the main feature of the Hospital Price Transparency module is that
regardless of the name of the uploaded file, the end user will always be
served a file that conforms with the guidelines from csm.gov. If you want to
customize the alias for hospital price transparency documents using pathauto,
it is highly recommended that your pattern END WITH the following:

`/[hpt:ein]_[hpt:hospital_name]_standardcharges.[hpt:fid:entity:extension]`

or much more succinctly (and equivalently):

`/[hpt:file_alais]`

However, you must add <code>hpt:file_alias</code> to the "safe patterns" on the
pathauto settings form.

If you use pathauto with other configurations, the end user may be served files
with names that do not conform to cms.gov regulations.
