<?php

namespace Drupal\hospital_price_transparency\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\PathElement;
use Drupal\Core\Url;

/**
 * Configuration form for a hospital price transparency entity type.
 */
class HospitalPriceTransparencySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hpt_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hospital_price_transparency.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hospital_price_transparency.settings');

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Please see <a href=":url" target="_blank">cms.gov</a> for guidelines and requirements pertaining to hospital price transparency.</p>', [':url' => 'https://www.cms.gov/files/document/steps-machine-readable-file.pdf']),
    ];

    $form['txt'] = [
      '#type' => 'link',
      '#title' => 'View your cms-hpt.txt file',
      '#url' => Url::fromUserInput('/cms-hpt.txt'),
      '#options' => [
        'attributes' => [
          'class' => [
            'button',
            'button--secondary',
          ],
        ],
      ],
    ];

    $form['source_page_url'] = [
      '#type' => 'path',
      '#convert_path' => PathElement::CONVERT_NONE,
      '#title' => $this->t('Source page path'),
      '#description' => $this->t('The local path to use when populating the <code>source-page-url</code> property in the <code>cms-hpt.txt</code> file.' ),
      '#default_value' => $config->get('source_page_url') ?? '',
    ];

    $form['contact_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default contact name'),
      '#description' => $this->t('Enter the contact name to be used for any HPT entity with an empty contact name field'),
      '#default_value' => $config->get('contact_name') ?? '',
    ];

    $form['contact_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default contact email'),
      '#description' => $this->t('Enter the contact email to be used for any HPT entity with an empty contact email field'),
      '#default_value' => $config->get('contact_email') ?? '',
    ];

    $form['allowed_file_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file types'),
      '#description' => $this->t('Enter space separated file extensions to allow. Standards from cms.gov require a machine readable content type such as xml, csv, or json. Some file types such as pdf would be a bad choice to allow.'),
      '#default_value' => $config->get('allowed_file_types'),
      '#required' => TRUE,
    ];

    $form['file_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File directory'),
      '#description' => $this->t('Customize where the file is stored. Regardless of where the file is stored, it will be served from an alias.'),
      '#default_value' => $config->get('file_directory'),
      '#required' => FALSE,
    ];

    $form['file_alias'] = [
      '#type' => 'radios',
      '#options' => [
        'text' => $this->t('Plain text'),
        'download' => $this->t('Direct download link'),
      ],
      '#title' => $this->t('"File alias" extra field settings'),
      '#description' => $this->t('You can configure the "File alias" extra field to display from the Manage Display tab.
        The form here allows you to configure how this field should render if it is used.'),
      '#default_value' => $config->get('file_alias'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hospital_price_transparency.settings');
    $config->set('source_page_url', $form_state->getValue('source_page_url'));
    $config->set('contact_name', $form_state->getValue('contact_name'));
    $config->set('contact_email', $form_state->getValue('contact_email'));
    $config->set('allowed_file_types', $form_state->getValue('allowed_file_types'));
    $config->set('file_directory', $form_state->getValue('file_directory'));
    $config->set('file_alias', $form_state->getValue('file_alias'));
    $config->save();
    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

}
