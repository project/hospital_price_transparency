<?php

namespace Drupal\hospital_price_transparency\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the hospital price transparency entity edit forms.
 */
class HospitalPriceTransparencyForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New hospital price transparency %label has been created.', $message_arguments));
        $this->logger('hospital_price_transparency')->notice('Created new hospital price transparency %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The hospital price transparency %label has been updated.', $message_arguments));
        $this->logger('hospital_price_transparency')->notice('Updated hospital price transparency %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.hpt.collection');

    return $result;
  }

}
