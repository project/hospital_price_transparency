<?php

namespace Drupal\hospital_price_transparency;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the hospital price transparency entity type.
 */
class HospitalPriceTransparencyListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new HospitalPriceTransparencyListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['txt'] = [
      '#type' => 'link',
      '#title' => 'View your cms-hpt.txt file',
      '#url' => Url::fromRoute('hospital_price_transparency.txt'),
      '#options' => [
        'attributes' => [
          'class' => [
            'button',
            'button--secondary',
          ],
        ],
      ],
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['ein'] = $this->t('EIN');
    $header['hospital_name'] = $this->t('Hospital Name');
    $header['changed'] = $this->t('Updated');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\hospital_price_transparency\HospitalPriceTransparencyInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    $row['ein'] = $entity->get('ein')->value;
    $row['hospital_name'] = $entity->get('hospital_name')->value;
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());
    $row['status'] = $entity->get('status')->value ? $this->t('Published') : $this->t('Unpublished');
    return $row + parent::buildRow($entity);
  }

}
