<?php

namespace Drupal\hospital_price_transparency;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the hospital price transparency entity type.
 */
class HospitalPriceTransparencyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIf(($account->hasPermission('access content') && $entity->isPublished()) || $account->hasPermission('edit hospital price transparency') || $account->hasPermission('administer hospital price transparency'))->cachePerPermissions()->addCacheableDependency($entity);

      case 'update':
      case 'view revision':
      case 'view all revisions':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['edit hospital price transparency', 'administer hospital price transparency'],
          'OR',
        )->cachePerPermissions()->addCacheableDependency($entity);;

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['delete hospital price transparency', 'administer hospital price transparency'],
          'OR',
        )->cachePerPermissions()->addCacheableDependency($entity);;

      case 'delete revision':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['delete hospital price transparency', 'administer hospital price transparency'],
          'OR',
        )->andIf(AccessResult::allowedIf(!$entity->isDefaultRevision() || !$entity->isLatestRevision()))->cachePerPermissions()->addCacheableDependency($entity);;

      case 'revert':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['edit hospital price transparency', 'administer hospital price transparency'],
          'OR',
        )->andIf(AccessResult::allowedIf(!$entity->isDefaultRevision() || !$entity->isLatestRevision()))->cachePerPermissions()->addCacheableDependency($entity);;

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      ['create hospital price transparency', 'administer hospital price transparency'],
      'OR',
    );
  }

}
