<?php

namespace Drupal\hospital_price_transparency\Entity;

use Drupal\Core\Archiver\Zip;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\hospital_price_transparency\HospitalPriceTransparencyInterface;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Defines the hospital price transparency entity class.
 *
 * @ContentEntityType(
 *   id = "hpt",
 *   label = @Translation("Hospital Price Transparency"),
 *   label_collection = @Translation("Hospital Price Transparency"),
 *   label_singular = @Translation("hospital price transparency"),
 *   label_plural = @Translation("hospital price transparency"),
 *   label_count = @PluralTranslation(
 *     singular = "@count hospital price transparency",
 *     plural = "@count hospital price transparency",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\hospital_price_transparency\HospitalPriceTransparencyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\hospital_price_transparency\HospitalPriceTransparencyAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\hospital_price_transparency\Form\HospitalPriceTransparencyForm",
 *       "edit" = "Drupal\hospital_price_transparency\Form\HospitalPriceTransparencyForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision-delete" = "\Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "\Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\hospital_price_transparency\Entity\Routing\HospitalPriceTransparencyRouteProvider",
 *       "revision" = "\Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "hpt",
 *   revision_table = "hpt_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer hpt",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/hpt",
 *     "add-form" = "/hpt/add",
 *     "canonical" = "/hpt/{hpt}",
 *     "edit-form" = "/hpt/{hpt}/edit",
 *     "delete-form" = "/hpt/{hpt}/delete",
 *     "revision" = "/hpt/{hpt}/revision/{hpt_revision}/view",
 *     "revision-delete-form" = "/hpt/{hpt}/revision/{hpt_revision}/delete",
 *     "revision-revert-form" = "/hpt/{hpt}/revision/{hpt_revision}/revert",
 *     "version-history" = "/hpt/{hpt}/revisions",
 *   },
 *   field_ui_base_route = "entity.hpt.settings",
 *   common_reference_target = TRUE,
 * )
 */
class HospitalPriceTransparency extends EditorialContentEntityBase implements HospitalPriceTransparencyInterface {

  /**
   * Extract zipped file, if applicable.
   *
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $file = $this->getFile();
    if ($file) {
      $filename = $file->getFilename();
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      if ($extension === 'zip') {
        // Validation has already been done by the HptZipConstraintValidator.
        $zip = new Zip(\Drupal::service('file_system')->realpath($file->getFileUri()));
        $zip_contents = $zip->listContents();
        foreach ($zip_contents as $zip_content) {
          if (!str_starts_with($zip_content, '__MACOSX') && $zip_content !== '.DS_STORE') {
            $zipped_filename = $zip_content;
            break;
          }
        }
        // Extract using realpath.
        $zip->extract(dirname(\Drupal::service('file_system')->realpath($file->getFileUri())), [$zipped_filename]);
        // Create file entity using uri.
        $zip_uri = $file->getFileUri();
        $unzipped_scheme = StreamWrapperManager::getScheme($zip_uri);
        $unzipped_dir = dirname(StreamWrapperManager::getTarget($zip_uri));
        $unzipped_dir_with_slash = $unzipped_dir === '.' ? '' : $unzipped_dir . '/';
        $unzipped_file_uri = $unzipped_scheme . '://' . $unzipped_dir_with_slash . $zipped_filename;
        $new_file = File::create([
          'uri' => $unzipped_file_uri,
        ]);
        $new_file->save();
        $this->set('fid', $new_file->id());
        \Drupal::messenger()->addStatus('The zipped file was automatically extracted and the archive was replaced.');
      }
    }
  }

  /**
   * Maintain an alias meeting cms.gov guidelines.
   *
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $path_alias_storage = \Drupal::service('entity_type.manager')->getStorage('path_alias');
    $aliases = $path_alias_storage->loadByProperties(['path' => '/hpt/' . $this->id()]);
    if ($aliases) {
      $alias = reset($aliases);
      $alias->setAlias('/hpt/' . $this->id() . '/' . $this->getFileAlias());
      $alias->save();
    }
    else {
      $alias = PathAlias::create([
        'path' => '/hpt/' . $this->id(),
        'alias' => '/hpt/' . $this->id() . '/' . $this->getFileAlias(),
      ]);
      $alias->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['ein'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('EIN'))
      ->setDescription(t('Enter the 9-digit Employer Identification Number (EIN) for the hospital.'))
      ->setRequired(TRUE)
      ->addPropertyConstraints('value', [
        'Regex' => [
          'pattern' => '/^\d{9}$/',
          'message' => new TranslatableMarkup('The EIN must be a 9-digit number with no hyphens.'),
        ],
      ])
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['hospital_name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Hospital name'))
      ->setDescription(t('Enter the name of the hospital as you would like it to appear in the filename. You may use letters, numbers, spaces, underscores, and hyphens.'))
      ->setRequired(TRUE)
      ->addPropertyConstraints('value', [
        'Regex' => [
          'pattern' => '/^(\w|\d| |-)+$/',
          'message' => new TranslatableMarkup('The Hospital name must only use letters, numbers, spaces, underscores, and hyphens.'),
        ],
      ])
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['contact_name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Contact name'))
      ->setDescription(t('The name of the point of contact designated to answer technical questions about the document'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hpt_contact_name',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['contact_email'] = BaseFieldDefinition::create('email')
      ->setRevisionable(TRUE)
      ->setLabel(t('Contact email'))
      ->setDescription(t('The email address designated to receive technical questions about the document'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hpt_contact_email',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['fid'] = BaseFieldDefinition::create('file')
      ->setRevisionable(TRUE)
      ->setLabel(t('File'))
      ->setRequired(TRUE)
      ->setSetting('file_extensions', 'xml csv json zip')
      ->setConstraints([
        'HptZipFileConstraint' => [],
      ])
      ->setDescription(t('The file containing hospital price transparency data. Regardless of the filename, it
        will be served to end users with a filename meeting cms.gov guidelines. If zip files are allowed, they must
        contain exactly one file that has an allowed extension.'))
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setSetting('off_label', 'Unpublished')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 1,
        'settings' => [
          'format' => 'default',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the hospital price transparency was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last updated'))
      ->setDescription(t('The time that the hospital price transparency was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Generate and return the filename meeting cms.gov guidelines.
   *
   * @return string
   */
  public function generateFilename() {
    $parts = [
      $this->get('ein')->getValue()[0]['value'],
      $this->get('hospital_name')->getValue()[0]['value'],
      'standardcharges',
    ];
    return implode("_", $parts);
  }

  /**
   * Get the url object for the hospital price transparency file
   *
   * @return Drupal\file\FileInterface|NULL
   */
  public function getFile() {
    return $this->entityTypeManager()->getStorage('file')->load($this->get('fid')->target_id);
  }

  /**
   * Get alias for the file per cms.gov conventions.
   */
  public function getFileAlias() {
    $file = $this->getFile();
    if ($file) {
      $filename = $file->getFilename();
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      return $this->generateFilename() . '.' . $extension;
    }
  }

}
