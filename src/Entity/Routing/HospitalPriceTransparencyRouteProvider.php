<?php

namespace Drupal\hospital_price_transparency\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for hpt entities.
 */
class HospitalPriceTransparencyRouteProvider extends AdminHtmlRouteProvider {

  /**
   * Allow collection access to anyone who can do anything with hpt entities.
   *
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);
    $permissions = [
      'administer hospital price transparency',
      'create hospital price transparency',
      'edit hospital price transparency',
      'delete hospital price transparency',
    ];
    $route->setRequirement('_permission', implode('+', $permissions));
    return $route;
  }

}
