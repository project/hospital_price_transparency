<?php

namespace Drupal\hospital_price_transparency\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Returns responses for Hospital Price Transparency routes.
 */
class HospitalPriceTransparencyController extends ControllerBase {

  /**
   * Serves contents of the hospital_price_transparency file.
   */
  public function file($hpt) {
    /** @var Drupal\hospital_price_transparency\HospitalPriceTransparencyInterface $hpt */
    $hpt = $this->entityTypeManager()->getStorage('hpt')->load($hpt);
    if ($hpt) {
      $access = $hpt->access('view', NULL, TRUE);
      if ($access->isAllowed()) {
        if ($file = $hpt->getFile()) {
          $file_access = $file->access('view', NULL, TRUE);
          if ($file_access->isAllowed()) {
            $response = new BinaryFileResponse($file->getFileUri());
            // Use fpassthru rather than file_get_contents to help with
            // memory allocation problems with huge files.
            $disposition = $response->headers->makeDisposition(
              ResponseHeaderBag::DISPOSITION_ATTACHMENT,
              $hpt->getFileAlias()
            );
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', $file->getMimeType());
            $response->headers->set('Content-Length', $file->getSize());
            return $response;
          }
          else {
            $response = new CacheableResponse($this->t('You are not allowed to view this Hospital Price Transparency document.'), Response::HTTP_FORBIDDEN);
            $response->addCacheableDependency($file_access);
            $response->addCacheableDependency($access);
            $response->addCacheableDependency($hpt);
            return $response;
          }
        }
      }
      else {
        $response = new CacheableResponse($this->t('You are not allowed to view this Hospital Price Transparency document.'), Response::HTTP_FORBIDDEN);
        $response->addCacheableDependency($access);
        $response->addCacheableDependency($hpt);
        return $response;
      }
    }
    $response = new CacheableResponse($this->t('The Hospital Price Transparency document was not found.'), Response::HTTP_NOT_FOUND);
    $response->addCacheableDependency($hpt);
    return $response;
  }

  /**
   * Serves the auto-generated cms-hpt.txt file.
   *
   * Note that a real file called cms-hpt.txt that exists in the webroot
   * will take precedence over this auto-generated version.
   *
   * @see https://cmsgov.github.io/hpt-tool/txt-generator/
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The cms-hpt.txt file as a response object with 'text/plain' content type.
   */
  public function txt() {
    $settings = \Drupal::config('hospital_price_transparency.settings');
    $source_page_url = NULL;
    if ($path = $settings->get('source_page_url')) {
      if (!str_starts_with($path, '/')) {
        $path = '/' . $path;
      }
      $source_page_url = Url::fromUserInput($path, ['absolute' => TRUE])->toString();
    }
    $content = [];
    $storage = $this->entityTypeManager()->getStorage('hpt');
    $hpts = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->execute();
    foreach ($hpts as $hpt) {
      if ($hpt = $storage->load($hpt)) {
        $data = [];
        $data['location-name'] = $hpt->label();
        $data['source-page-url'] = $source_page_url;
        $data['mrf-url'] = $hpt->toUrl('canonical', ['absolute' => TRUE])->toString();
        $data['contact-name'] = $hpt->get('contact_name')->getString() ?: $settings->get('contact_name');
        $data['contact-email'] = $hpt->get('contact_email')->getString() ?: $settings->get('contact_email');
        $this->moduleHandler()->alter('hospital_price_transparency_txt', $data, $hpt);
        foreach ($data as $key => $value) {
          $content[] = "$key: $value";
        }
        $content[] = '';
      }
    }
    $content = implode("\n", $content);
    $response = new CacheableResponse($content, Response::HTTP_OK, ['content-type' => 'text/plain']);
    $meta_data = $response->getCacheableMetadata();
    $meta_data->addCacheTags([
      'hpt_list',
      'config:hospital_price_transparency.settings',
    ]);
    $meta_data->addCacheContexts(['url.site']);
    return $response;
  }

}
