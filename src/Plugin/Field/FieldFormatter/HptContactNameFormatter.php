<?php

namespace Drupal\hospital_price_transparency\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;

/**
 * Plugin implementation of the 'hpt_contact_name' formatter.
 *
 * @FieldFormatter(
 *   id = "hpt_contact_name",
 *   label = @Translation("Contact name with default"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class HptContactNameFormatter extends BasicStringFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    if (empty($elements)) {
      if ($default = \Drupal::config('hospital_price_transparency.settings')->get('contact_name')) {
        $elements[] = [
          '#type' => 'inline_template',
          '#template' => '{{ value|nl2br }}',
          '#context' => ['value' => $default],
          '#cache' => [
            'tags' => ['config:hospital_price_transparency.settings'],
          ],
        ];
      }
      $elements['#cache'] = [
        'tags' => ['config:hospital_price_transparency.settings'],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return ($field_definition->getName() === 'contact_name')
      && ($field_definition->getTargetEntityTypeId() === 'hpt');
  }

}
