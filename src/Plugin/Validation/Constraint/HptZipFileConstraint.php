<?php

namespace Drupal\hospital_price_transparency\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that an uploaded zip file is valid for hpt.
 *
 * @Constraint(
 *   id = "HptZipFileConstraint",
 *   label = @Translation("HPT Zip File Contstraint", context = "Validation"),
 *   type = "file"
 * )
 */
class HptZipFileConstraint extends Constraint {

  /**
   * Too many files.
   *
   * @var string
   */
  public $tooManyFiles = 'Zip file must contain exactly one file. Uploaded zip file contains %count files.';

  /**
   * Invalid file type.
   *
   * @var string
   */
  public $invalidFileType = 'Uploaded zips must contain a file with the following extensions: %files-allowed.';

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return '\Drupal\hospital_price_transparency\Plugin\Validation\Constraint\HptZipFileConstraintValidator';
  }

}
