<?php

namespace Drupal\hospital_price_transparency\Plugin\Validation\Constraint;

use Drupal\Core\Archiver\Zip;
use Drupal\Core\Entity\EntityTypeManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Checks that an uploaded zip file is valid for hpt.
 */
class HptZipFileConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * File storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Create an instance of the validator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(EntityTypeManager $entity_type_manager, LoggerInterface $logger) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('hospital_price_transparency')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      if ($id = $item->target_id ?? NULL) {
        $file = $this->fileStorage->load($id);
        if ($file) {
          $filename = $file->getFilename();
          $extension = pathinfo($filename, PATHINFO_EXTENSION);
          if ($extension === 'zip') {
            $zip = new Zip(\Drupal::service('file_system')->realpath($file->getFileUri()));
            $zip_contents = $zip->listContents();
            $filtered_zip_contents = [];
            foreach ($zip_contents as $zip_content) {
              if (!str_starts_with($zip_content, '__MACOSX') && $zip_content !== '.DS_STORE') {
                $filtered_zip_contents[] = $zip_content;
              }
            }
            if (count($filtered_zip_contents) !== 1) {
              $this->context->addViolation($constraint->tooManyFiles, ['%count' => count($filtered_zip_contents)]);
            }
            $zipped_filename = reset($filtered_zip_contents);
            $allowed_extensions = $items->getFieldDefinition()->getSetting('file_extensions');
            $allowed_extensions = trim(str_replace('zip', '', $allowed_extensions));
            $regex = '/\.(' . preg_replace('/ +/', '|', preg_quote($allowed_extensions)) . ')$/i';
            if (!preg_match($regex, $zipped_filename)) {
              $this->context->addViolation($constraint->invalidFileType, ['%files-allowed' => $allowed_extensions]);
            }
          }
        }
      }
    }
  }

}
